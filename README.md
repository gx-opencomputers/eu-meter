### About eu-meter
![](doc/pictures/view.png)

It is just a simple [OpenComputers](http://ocdoc.cil.li/) script for
energy monitoring purpose. You can even control your energy flow if
you want to by little code modification.

Supports only [IndustrialCraft²](http://wiki.industrial-craft.net)
[EU storage blocks](http://wiki.industrial-craft.net/index.php?title=EU_storage_block)
(BatBox, CESU, MFE, MFSU).

### Configuration requirements and HOWTO

Mods components:
  - OpenComputers _(tested 1.5.x, 1.6.x)_
  - IndustrialCraft²

System configuration:
  - Working computer with _OpenOS_
  - [Adapter](http://ocdoc.cil.li/block:adapter) between each storage
  block and OpenComputer's network, [cable](http://ocdoc.cil.li/block:cable)
  is your best friend

![](doc/pictures/settings.png)

Put `eu-meter.lua` script into your computer and just run with
> ./eu-meter.lua

If you have [internet card](http://ocdoc.cil.li/item:internet_card)
installed you can simply download it directly
(use `MMB` or `insert` key to paste from clipboard)
> wget https://gitlab.com/gx-opencomputers/eu-meter/raw/master/eu-meter.lua

Script will automatically scan supported devices and monitor them in
each refresh sequence - hot-plugging new storage blocks into network
are safe but disconnecting may cause script crash.
Look into script header where you can find a few variables to configure
(like refresh-rate, display resolution, default font color), all of them
are well documented.