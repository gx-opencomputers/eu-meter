local sleep_time = 0.5 -- time between screen refresh
local res_x, res_y = 43, 10 -- screen resolution, don't change if you don't know what are you doing
local text_color = 0x00aaff -- default font RGB color value
local peak_cache_size = 2 -- how many samples is used to calculate average peak value
-- End of configuration section

local component = require("component")
local sleep = os.sleep
local floor = math.floor


local bar = {}
bar.b1, bar.b2, bar.b3, bar.b4, bar.b5, bar.b6, bar.b7, bar.b8 = '▏', '▎', '▍', '▌', '▋', '▊', '▉', '█'
bar.start_char, bar.end_char = '║', '║'

component.gpu.setResolution(res_x, res_y)

function create_cache(size)
    local cache = {}
    for i=1,size do
        table.insert(cache, 0)
    end
    return cache
end

function update_cache(cache, value)
    table.insert(cache, value)
    table.remove(cache, 1)
end

function average_from_cache(cache)
    local length = 0
    local sum = 0

    for i, val in pairs(cache) do
        sum = sum + val
        length = length + 1
    end

    return sum / length
end

function multiply_str(str, times)
    if times == 1 then
        return str
    elseif times == 0 then
        return ''
    else
        local created = ''
        for i=1,times do
            created = created .. str
        end
        return created
    end
end

function draw_progressbar(progress)
    local bar_str = bar.start_char
    local blocks_space = res_x - 3 - 7
    local full_blocks = floor(blocks_space / 100 * progress)
    bar_str = bar_str .. multiply_str(bar.b8, full_blocks)
    local rest = progress % (100 / blocks_space) / (100 / blocks_space)
    if progress < 100 then
        if rest < 0.125 then
            bar_str = bar_str .. ' '
        elseif rest < 0.250 then
            bar_str = bar_str .. bar.b1
        elseif rest < 0.375 then
            bar_str = bar_str .. bar.b2
        elseif rest < 0.500 then
            bar_str = bar_str .. bar.b3
        elseif rest < 0.625 then
            bar_str = bar_str .. bar.b4
        elseif rest < 0.750 then
            bar_str = bar_str .. bar.b5
        elseif rest < 0.875 then
            bar_str = bar_str .. bar.b6
        elseif rest < 1.0 then
            bar_str = bar_str .. bar.b7
        end
    end
    bar_str = bar_str .. multiply_str(' ', (blocks_space) - full_blocks - 1) .. bar.end_char
    bar_str = bar_str .. ' ' .. progress .. '%'
    return bar_str
end

function colorize(value)
    local red, green, blue = 0, 0, 0
    if value <= 50 then
        red = 255
        green = floor(value / 50 * 255)
    elseif value > 50 then
        red = floor((100 - value) / 50 * 255)
        green = 255
    end
    return (red * 256 * 256) + (green * 256) + blue
end

function fill(str, length, char, side)
    -- fill given string to given length with given char
    -- side define side of additional chars, 0 for left, 1 for right
    local str = str
    local side = side or 1
    if side < 0 or side > 1 then
        error('give proper side int value (0 or 1)')
    end
    local string_length = string.len(str)
    for i=string_length+1,length do
        if side == 0 then
            str = char .. str
        elseif side == 1 then
            str = str .. char
        end
    end
    return str
end

function colorize_peak(value, step)
    local red, green, blue = 0, 0, 0
    if value > 0 and value < step then
        red = 255 - floor(value / step * 255)
        green = 255
    elseif value < 0 and value > -step then
        red = 255
        green = 255 - (floor(value / step * 255) * -1)
    elseif value >= step then
        red = 0
        green = 255
    elseif value <= -step then
        red = 255
        green = 0
    elseif value == 0 then
        red = 255
        green = 255
    end

    return (red * 256 * 256) + (green * 256) + blue
end

function colorize_state(state_str)
    if state_str == "charged" or state_str == "charging" then
        return 0x00ff00
    elseif state_str == "idle" then
        return 0xffff00
    elseif state_str == "discharging" then
        return 0xff0000
    else
        return 0xffffff
    end
end

function round(num, idp)
    local mult = 10 ^ (idp or 0)
    return math.floor(num * mult + 0.5) / mult
end

function detect_energy_storages()
    local energy_storages = {}
    for adress, device in component.list() do
        if device == "mfsu" or device == "mfe" or device == "cesu" or device == "batbox" then
            table.insert(energy_storages, adress)
        end
    end
    return energy_storages
end

-- Let's the code begin
local peak_cache = create_cache(peak_cache_size)
local old_energy = 0

while true do
    local storages = detect_energy_storages()

    local energy = 0
    local capacity = 0
    local storages_count = 0
    local status_str, peak_str

    for i, addr in pairs(storages) do
        energy = energy + component.invoke(addr, "getEUStored")
        capacity = capacity + component.invoke(addr, "getEUCapacity")
        storages_count = storages_count + 1
    end

    if energy >= capacity - 20000 then
        status_str = "charged"
        energy = capacity -- for elegant output
    else
        if energy > old_energy then
            status_str = "charging"
        elseif energy == old_energy then
            status_str = "idle"
        else
            status_str = "discharging"
        end
    end

    local peak_energy = floor((energy - old_energy) / 1000 / sleep_time) -- todo: fix peaK_energy start display

    update_cache(peak_cache, peak_energy)
    local avg_peak_energy = floor(average_from_cache(peak_cache))

    local capacity_str = floor(capacity / 1000) .. " kEU"
    local energy_str = floor(energy / 1000) .. " kEU"
    energy_str = fill(energy_str, string.len(capacity_str), ' ', 0)

    if avg_peak_energy > 0 then
        peak_str = '+' .. avg_peak_energy .. ' kEU/s'
    else
        peak_str = avg_peak_energy .. ' kEU/s'
    end
    peak_str = fill(peak_str, string.len(capacity_str) + 2, ' ', 0)
    storages_count = fill(storages_count, string.len(capacity_str) - 4, ' ', 0)

    old_energy = energy
    local percentage_energy = round(energy / capacity * 100, 2)

    -- START DRAWING SECTION
    component.gpu.setForeground(text_color)
    print(os.date(), "-- UGX®EMS --")
    print("(Ultimate GX® Energy Monitoring System)")
    component.gpu.setForeground(colorize(percentage_energy))
    print(draw_progressbar(percentage_energy))
    component.gpu.setForeground(colorize_state(status_str))
    print("state:", '', '', status_str)
    component.gpu.setForeground(colorize_peak(avg_peak_energy, 100))
    print("peak:", '', '', peak_str)
    component.gpu.setForeground(text_color)
    -- print("percentage energy:", percentage_energy .. "%")
    print("all stored energy:", energy_str)
    component.gpu.setForeground(text_color)
    print("maximum capacity:", capacity_str)
    print("energy storages:", storages_count)
    if excess == 'True' then
        component.gpu.setForeground(0xff0000)
    elseif excess == 'False' then
        component.gpu.setForeground(0x00ff00)
    end
    print()
    component.gpu.setForeground(text_color)
    -- END DRAWING SECTION

    sleep(sleep_time)
end